package tk.mikazuki;

import com.google.common.base.Function;
import ninja.leaping.configurate.ConfigurationNode;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class IntelliJapaneseConfig {

    private Database database;

    private TextFile textFile;

    private IjConfig google;

    private Yahoo yahoo;

    private IjConfig social;

    private IjConfig mikugo;

    private Ignored ignored;

    public IntelliJapaneseConfig(ConfigurationNode root) {
        this.database = new Database(root.getNode("config", "local", "database"));
        this.textFile = new TextFile(root.getNode("config", "local", "text"));
        this.google = new IjConfig(root.getNode("config", "online", "google"));
        this.yahoo = new Yahoo(root.getNode("config", "online", "yahoo"));
        this.social = new IjConfig(root.getNode("config", "online", "social"));
        this.mikugo = new IjConfig(root.getNode("config", "online", "mikugo"));
        this.ignored = new Ignored(root.getNode("config", "ignored"));
    }

    public Database getDatabaseConfig() {
        return database;
    }

    public TextFile getTextFileConfig() {
        return textFile;
    }

    public IjConfig getGoogleConfig() {
        return google;
    }

    public Yahoo getYahooConfig() {
        return yahoo;
    }

    public IjConfig getSocialConfig() {
        return social;
    }

    public IjConfig getMikugoConfig() {
        return mikugo;
    }

    public Ignored getIgnoredConfig() {
        return ignored;
    }

    public class IjConfig {

        protected ConfigurationNode root;

        private boolean isEnable = false;

        public IjConfig(ConfigurationNode node) {
            this.root = node;

            this.isEnable = this.root.getNode("enable").getBoolean();
        }

        public boolean isEnable() {
            return isEnable;
        }
    }

    public class Database extends IjConfig {

        private String url;

        private String username;

        private String password;

        public Database(ConfigurationNode node) {
            super(node);

            this.url = this.root.getNode("url").getString();
            this.username = this.root.getNode("user").getString();
            this.password = this.root.getNode("pass").getString();
        }

        public String getUrl() {
            return url;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }

    public class TextFile extends IjConfig {

        private String url;

        public TextFile(ConfigurationNode node) {
            super(node);

            this.url = this.root.getNode("url").getString();
        }

        public String getUrl() {
            return url;
        }
    }

    public class Yahoo extends IjConfig {

        private String appid;

        public Yahoo(ConfigurationNode node) {
            super(node);
            this.appid = this.root.getNode("appid").getString();
        }

        public String getAppid() {
            return appid;
        }
    }

    public class Ignored {

        private List<Pattern> patterns = new ArrayList<Pattern>();

        public Ignored(ConfigurationNode root) {
            root.getNode("list").getList(new Function<Object, Object>() {
                @Nullable
                @Override
                public Object apply(Object input) {
                    patterns.add(Pattern.compile(input.toString()));
                    return null;
                }
            });
        }

        public List<Pattern> getPatterns() {
            return patterns;
        }
    }
}
