package tk.mikazuki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RomaToHira {

    private Map<String, String> table;

    private List<String> reMapped;

    private static RomaToHira instance = new RomaToHira();

    private RomaToHira() {
        this.table = new HashMap<>();
        this.reMapped = new ArrayList<>();

        this.table.put("a", "あ");
        this.table.put("i", "い");
        this.table.put("u", "う");
        this.table.put("e", "え");
        this.table.put("o", "お");
        this.table.put("ka", "か");
        this.table.put("ki", "き");
        this.table.put("ku", "く");
        this.table.put("ke", "け");
        this.table.put("ko", "こ");
        this.table.put("sa", "さ");
        this.table.put("si", "し");
        this.table.put("shi", "し");
        this.table.put("su", "す");
        this.table.put("se", "せ");
        this.table.put("so", "そ");
        this.table.put("ta", "た");
        this.table.put("ti", "ち");
        this.table.put("tu", "つ");
        this.table.put("tsu", "つ");
        this.table.put("te", "て");
        this.table.put("to", "と");
        this.table.put("na", "な");
        this.table.put("ni", "に");
        this.table.put("nu", "ぬ");
        this.table.put("ne", "ね");
        this.table.put("no", "の");
        this.table.put("ha", "は");
        this.table.put("hi", "ひ");
        this.table.put("hu", "ふ");
        this.table.put("fu", "ふ");
        this.table.put("he", "へ");
        this.table.put("ho", "ほ");
        this.table.put("ma", "ま");
        this.table.put("mi", "み");
        this.table.put("mu", "む");
        this.table.put("me", "め");
        this.table.put("mo", "も");
        this.table.put("ya", "や");
        this.table.put("yu", "ゆ");
        this.table.put("yo", "よ");
        this.table.put("ra", "ら");
        this.table.put("ri", "り");
        this.table.put("ru", "る");
        this.table.put("re", "れ");
        this.table.put("ro", "ろ");
        this.table.put("wha", "うぁ");
        this.table.put("whi", "うぃ");
        this.table.put("wi", "うぃ");
        this.table.put("whe", "うぇ");
        this.table.put("we", "うぇ");
        this.table.put("who", "うぉ");
        this.table.put("kya", "きゃ");
        this.table.put("kyi", "きぃ");
        this.table.put("kyu", "きゅ");
        this.table.put("kye", "きぇ");
        this.table.put("kyo", "きょ");
        this.table.put("sya", "しゃ");
        this.table.put("sha", "しゃ");
        this.table.put("syi", "しぃ");
        this.table.put("syu", "しゅ");
        this.table.put("shu", "しゅ");
        this.table.put("sye", "しぇ");
        this.table.put("she", "しぇ");
        this.table.put("syo", "しょ");
        this.table.put("sho", "しょ");
        this.table.put("cha", "ちゃ");
        this.table.put("cya", "ちゃ");
        this.table.put("tya", "ちゃ");
        this.table.put("cyi", "ちぃ");
        this.table.put("tyi", "ちぃ");
        this.table.put("chu", "ちゅ");
        this.table.put("cyu", "ちゅ");
        this.table.put("tyu", "ちゅ");
        this.table.put("che", "ちぇ");
        this.table.put("cye", "ちぇ");
        this.table.put("tye", "ちぇ");
        this.table.put("cho", "ちょ");
        this.table.put("cyo", "ちょ");
        this.table.put("tsa", "つぁ");
        this.table.put("tsi", "つぃ");
        this.table.put("tse", "つぇ");
        this.table.put("tso", "つぉ");
        this.table.put("tha", "てゃ");
        this.table.put("thi", "てぃ");
        this.table.put("thu", "てゅ");
        this.table.put("the", "てぇ");
        this.table.put("tho", "てょ");
        this.table.put("nya", "にゃ");
        this.table.put("nyi", "にぃ");
        this.table.put("nyu", "にゅ");
        this.table.put("nye", "にぇ");
        this.table.put("nyo", "にょ");
        this.table.put("hya", "ひゃ");
        this.table.put("hyi", "ひぃ");
        this.table.put("hyu", "ひゅ");
        this.table.put("hye", "ひぇ");
        this.table.put("hyo", "ひょ");
        this.table.put("mya", "みゃ");
        this.table.put("myi", "みぃ");
        this.table.put("myu", "みゅ");
        this.table.put("mye", "みぇ");
        this.table.put("myo", "みょ");
        this.table.put("rya", "りゃ");
        this.table.put("ryi", "りぃ");
        this.table.put("ryu", "りゅ");
        this.table.put("rye", "りぇ");
        this.table.put("ryo", "りょ");
        this.table.put("xa", "ぁ");
        this.table.put("la", "ぁ");
        this.table.put("xi", "ぃ");
        this.table.put("li", "ぃ");
        this.table.put("xu", "ぅ");
        this.table.put("lu", "ぅ");
        this.table.put("xe", "ぇ");
        this.table.put("le", "ぇ");
        this.table.put("xo", "ぉ");
        this.table.put("lo", "ぉ");
        this.table.put("ga", "が");
        this.table.put("gi", "ぎ");
        this.table.put("gu", "ぐ");
        this.table.put("ge", "げ");
        this.table.put("go", "ご");
        this.table.put("za", "ざ");
        this.table.put("zi", "じ");
        this.table.put("ji", "じ");
        this.table.put("zu", "ず");
        this.table.put("ze", "ぜ");
        this.table.put("zo", "ぞ");
        this.table.put("da", "だ");
        this.table.put("di", "ぢ");
        this.table.put("du", "づ");
        this.table.put("de", "で");
        this.table.put("do", "ど");
        this.table.put("xtu", "っ");
        this.table.put("ltu", "っ");
        this.table.put("ba", "ば");
        this.table.put("bi", "び");
        this.table.put("bu", "ぶ");
        this.table.put("be", "べ");
        this.table.put("bo", "ぼ");
        this.table.put("pa", "ぱ");
        this.table.put("pi", "ぴ");
        this.table.put("pu", "ぷ");
        this.table.put("pe", "ぺ");
        this.table.put("po", "ぽ");
        this.table.put("xya", "ゃ");
        this.table.put("lya", "ゃ");
        this.table.put("xyu", "ゅ");
        this.table.put("lyu", "ゅ");
        this.table.put("xyo", "ょ");
        this.table.put("lyo", "ょ");
        this.table.put("wa", "わ");
        this.table.put("wo", "を");
        this.table.put("nn", "ん");
        this.table.put("n", "ん");
        this.table.put("xwa", "ゎ");
        this.table.put("va", "ヴぁ");
        this.table.put("vi", "ヴぃ");
        this.table.put("vu", "ヴ");
        this.table.put("ve", "ヴぇ");
        this.table.put("vo", "ヴぉ");
        this.table.put("gya", "ぎゃ");
        this.table.put("gyi", "ぎぃ");
        this.table.put("gyu", "ぎゅ");
        this.table.put("gye", "ぎぇ");
        this.table.put("gyo", "ぎょ");
        this.table.put("zya", "じゃ");
        this.table.put("jya", "じゃ");
        this.table.put("ja", "じゃ");
        this.table.put("jyi", "じぃ");
        this.table.put("zyi", "じぃ");
        this.table.put("zyu", "じゅ");
        this.table.put("jyu", "じゅ");
        this.table.put("ju", "じゅ");
        this.table.put("zye", "じぇ");
        this.table.put("jye", "じぇ");
        this.table.put("je", "じぇ");
        this.table.put("zyo", "じょ");
        this.table.put("jyo", "じょ");
        this.table.put("jo", "じょ");
        this.table.put("dya", "ぢゃ");
        this.table.put("dyi", "ぢぃ");
        this.table.put("dyu", "ぢゅ");
        this.table.put("dye", "ぢぇ");
        this.table.put("dyo", "ぢょ");
        this.table.put("dha", "でゃ");
        this.table.put("dhi", "でぃ");
        this.table.put("dhu", "でゅ");
        this.table.put("dhe", "でぇ");
        this.table.put("dho", "でょ");
        this.table.put("bya", "びゃ");
        this.table.put("byi", "びぃ");
        this.table.put("byu", "びゅ");
        this.table.put("bye", "びぇ");
        this.table.put("byo", "びょ");
        this.table.put("pya", "ぴゃ");
        this.table.put("pyi", "ぴぃ");
        this.table.put("pyu", "ぴゅ");
        this.table.put("pye", "ぴぇ");
        this.table.put("pyo", "ぴょ");
        this.table.put("fa", "ふぁ");
        this.table.put("fi", "ふぃ");
        this.table.put("fe", "ふぇ");
        this.table.put("fo", "ふぉ");
        this.table.put("gwa", "ぐぁ");
        this.table.put("gwi", "ぐぃ");
        this.table.put("gwu", "ぐぅ");
        this.table.put("gwe", "ぐぇ");
        this.table.put("gwo", "ぐぉ");

        this.reMapped.add("sh");
        this.reMapped.add("ts");
        this.reMapped.add("wh");
        this.reMapped.add("ky");
        this.reMapped.add("sy");
        this.reMapped.add("ch");
        this.reMapped.add("cy");
        this.reMapped.add("ty");
        this.reMapped.add("th");
        this.reMapped.add("ny");
        this.reMapped.add("hy");
        this.reMapped.add("my");
        this.reMapped.add("ry");
        this.reMapped.add("xt");
        this.reMapped.add("lt");
        this.reMapped.add("xy");
        this.reMapped.add("ly");
        this.reMapped.add("gy");
        this.reMapped.add("zy");
        this.reMapped.add("jy");
        this.reMapped.add("dy");
        this.reMapped.add("dh");
        this.reMapped.add("by");
        this.reMapped.add("py");
        this.reMapped.add("gw");
    }

    public static RomaToHira getSingleton() {
        return instance;
    }

    // TODO: Method "getConvertText(String)" is too long.
    public String getConvertText(String text) {
        Map<String, String> re = new HashMap<>();
        int counter = 0;
        for (Pattern pattern : IntelliJapanese.getInstance().getConfig().getIgnoredConfig().getPatterns()) {
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                re.put("%" + counter + "%", matcher.group());
                text = text.replaceAll(pattern.pattern(), "%" + (counter++) + "%");
            }
        }

        StringBuilder replaced = new StringBuilder();
        text = text.toLowerCase();

        for (int index = 0; index < text.length(); index++) {
            int endIndex = index + 3;
            if (endIndex > text.length()) {
                endIndex = text.length();
            }
            String value = text.substring(index, endIndex);
            if (this.table.containsKey(value)) {
                replaced.append(this.table.get(value));
                index = endIndex - 1;
                continue;
            }

            if (value.length() >= 2) {
                // "っ" のチェック
                if (value.charAt(0) == value.charAt(1)) {
                    if (value.charAt(0) != 'a' && value.charAt(0) != 'i' && value.charAt(0) != 'u' &&
                        value.charAt(0) != 'e' && value.charAt(0) != 'o' && value.charAt(0) != 'n' &&
                        ('a' <= value.charAt(0) && value.charAt(0) <= 'z')) {
                        replaced.append("っ");
                        index++;
                        endIndex++;
                    }
                }
            }

            // 2文字, 1文字...
            boolean flag = false;
            boolean reMapped = false;
            for (int i = 0; i < 2; i++) {
                endIndex--;
                if (endIndex < 0 || endIndex < index) {
                    break;
                }
                value = text.substring(index, endIndex);
                if (this.reMapped.contains(value)) {
                    reMapped = true;
                    break;
                }
                if (this.table.containsKey(value)) {
                    replaced.append(this.table.get(value));
                    index = endIndex - 1;
                    flag = true;
                    break;
                }
            }
            if (flag) {
                continue;
            }
            if (reMapped) {
                index--;
                continue;
            }

            // Not Found(´・ω・`)
            replaced.append(text.substring(index, index + 1));
        }

        String value = replaced.toString();
        for (String string : re.keySet()) {
            value = value.replaceAll(string, re.get(string));
        }

        return value;
    }
}