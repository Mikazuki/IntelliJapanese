package tk.mikazuki.commands;

import org.spongepowered.api.Game;
import org.spongepowered.api.service.sql.SqlService;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.source.CommandBlockSource;
import org.spongepowered.api.util.command.spec.CommandExecutor;
import tk.mikazuki.IntelliJapanese;
import tk.mikazuki.converter.local.DatabaseConverter;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by Mikazuki on 2015/05/10.
 */
public class UnRegisterCommand implements CommandExecutor {

    private final SqlService sqlService;
    private final String url;
    private final DatabaseConverter converter;

    public UnRegisterCommand(Game game, DatabaseConverter converter) {
        this.sqlService = game.getServiceManager().provide(SqlService.class).get();
        this.converter = converter;
        String url = IntelliJapanese.getInstance().getConfigDir() + IntelliJapanese.getInstance()
            .getConfig()
            .getDatabaseConfig()
            .getUrl();
        url = url.replace(".mv.db", "");
        this.url = url.replace(".h2.db", "");
    }

    /**
     * Callback for the execution of a command.
     *
     * @param src  The commander who is executing this command
     * @param args The parsed command arguments for this command
     * @return the result of executing this command
     * @throws CommandException If a user-facing error occurs while executing this command
     */
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof CommandBlockSource) {
            return CommandResult.empty();
        }

        String value1 = args.<String>getOne("value1").get();
        try (Connection connection = this.sqlService.getDataSource("jdbc:h2:" + this.url).getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM DICTIONARY WHERE TARGET = ?")) {
            statement.setString(1, value1);

            statement.execute();
            this.converter.refresh();
        }
        catch (Exception e) {
            e.printStackTrace();
            return CommandResult.empty();
        }
        return CommandResult.success();
    }
}
