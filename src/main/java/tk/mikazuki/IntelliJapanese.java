package tk.mikazuki;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.event.Subscribe;
import org.spongepowered.api.event.entity.player.PlayerChatEvent;
import org.spongepowered.api.event.state.InitializationEvent;
import org.spongepowered.api.event.state.PostInitializationEvent;
import org.spongepowered.api.event.state.PreInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.config.DefaultConfig;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.util.command.args.GenericArguments;
import org.spongepowered.api.util.command.spec.CommandSpec;
import tk.mikazuki.commands.RegisterCommand;
import tk.mikazuki.commands.UnRegisterCommand;
import tk.mikazuki.converter.local.DatabaseConverter;
import tk.mikazuki.converter.local.FileConverter;
import tk.mikazuki.converter.local.LocalConverter;
import tk.mikazuki.converter.online.*;
import tk.mikazuki.exception.InvalidConfigException;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("SpellCheckingInspection")
@Plugin(id = "IntelliJapanese", name = "Intelli Japanese")
public class IntelliJapanese {

    private static IntelliJapanese instance;

    private IntelliJapaneseConfig config;

    private boolean isEnable;

    private LocalConverter localConverter;
    private OnlineConverter onlineConverter;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private File defaultConfig;

    private String configDir;

    public IntelliJapanese() {
        instance = this;
        this.isEnable = true;
    }

    @Subscribe
    public void onPreInitialization(PreInitializationEvent event) {
        if (!defaultConfig.exists()) {
            this.generateDefaultConfig();
        }
        this.configDir = this.defaultConfig.getParent();

        ConfigurationNode root;
        ConfigurationLoader<CommentedConfigurationNode> configManager = HoconConfigurationLoader.builder()
            .setFile(this.defaultConfig)
            .build();
        try {
            root = configManager.load();
        }
        catch (Exception e) {
            root = null;
        }
        if (root == null) {
            this.isEnable = false;
            return;
        }

        this.config = new IntelliJapaneseConfig(root);
    }

    @Subscribe
    public void onInitialization(InitializationEvent event) throws Exception {
        if (!this.isEnable) {
            return;
        }
        // local
        if (this.config.getDatabaseConfig().isEnable()) {
            this.localConverter = new DatabaseConverter(event.getGame(), this.config.getDatabaseConfig().getUrl());
            // commands
            HashMap<List<String>, CommandSpec> subCommands = new HashMap<>();
            subCommands.put(Arrays.asList("register", "r"),
                            CommandSpec.builder()
                                .setPermission("intellijapanese.commands.register")
                                .setDescription(Texts.of("Register a phrase."))
                                .setArguments(GenericArguments.seq(GenericArguments.string(Texts.of("value1")),
                                                                   GenericArguments.string(Texts.of("value2"))))
                                .setExecutor(new RegisterCommand(event.getGame(),
                                                                 (DatabaseConverter) this.localConverter))
                                .build());
            subCommands.put(Arrays.asList("unregister", "u"),
                            CommandSpec.builder()
                                .setPermission("intellijapanese.commands.unregister")
                                .setDescription(Texts.of("Unregister a phrase."))
                                .setArguments(GenericArguments.seq(GenericArguments.string(Texts.of("value1"))))
                                .setExecutor(new UnRegisterCommand(event.getGame(),
                                                                   (DatabaseConverter) this.localConverter))
                                .build());

            CommandSpec commandSpec = CommandSpec.builder()
                .setDescription(Texts.of("Dictionary Management Command"))
                .setPermission("intellijapanese.commands")
                .setChildren(subCommands)
                .build();

            event.getGame().getCommandDispatcher().register(this, commandSpec, "ijdict");
        }
        else if (this.config.getTextFileConfig().isEnable()) {
            this.localConverter = new FileConverter(this.config.getTextFileConfig().getUrl());
        }
        else {
            throw new InvalidConfigException("You must be enabled DB or Text.");
        }

        // online
        if (this.config.getGoogleConfig().isEnable()) {
            this.onlineConverter = new GoogleConverter();
        }
        else if (this.config.getYahooConfig().isEnable()) {
            this.onlineConverter = new YahooConverter(this.config.getYahooConfig().getAppid());
        }
        else if (this.config.getSocialConfig().isEnable()) {
            this.onlineConverter = new SocialConverter();
        }
        else if (this.config.getMikugoConfig().isEnable()) {
            this.onlineConverter = new MikugoConverter();
        }
        else {
            throw new InvalidConfigException("You must be enabled one of Google, Yahoo, Social or Mikugo");
        }
    }

    @Subscribe
    public void onPostInitialization(PostInitializationEvent event) {
        //
    }

    @Subscribe
    public void onPlayerChat(PlayerChatEvent event) {
        if (!this.isEnable) {
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();
        ImmutableList<Object> immutableList = ((Text.Translatable) event.getMessage()).getArguments();
        // getArguments()[0] is Player Display Name.
        // e.g. mikazuki.
        for (int i = 1; i < immutableList.size(); i++) {
            Object object = immutableList.get(i);
            if (object instanceof Text.Literal) {
                Text literal = (Text) object;
                for (Object innerObj : literal.getChildren()) {
                    if (innerObj instanceof Text.Literal) {
                        String value = this.localConverter.getRomaString(((Text.Literal) innerObj).getContent());
                        value = RomaToHira.getSingleton().getConvertText(value);
                        value = this.onlineConverter.getString(value);
                        value = this.localConverter.getConvertedString(value);
                        stringBuilder.append(value);
                    }
                }
            }
        }

        // Send Translated Message
        event.getGame()
            .getServer()
            .broadcastMessage(Texts.of(TextColors.LIGHT_PURPLE,
                                       "[IntelliJapanese]",
                                       TextColors.WHITE,
                                       String.format(" <%s> ", event.getUser().getName()),
                                       TextColors.GOLD,
                                       TextStyles.ITALIC,
                                       stringBuilder.toString()));
    }

    public IntelliJapaneseConfig getConfig() {
        return this.config;
    }

    private void generateDefaultConfig() {
        try (InputStream is = this.getClass().getResourceAsStream("/defaultConfig.conf");
             BufferedReader br = new BufferedReader(new InputStreamReader(is));
             BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.defaultConfig)))) {

            String line;
            while ((line = br.readLine()) != null) {
                bw.write(line + "\n");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getConfigDir() {
        return configDir;
    }

    public static IntelliJapanese getInstance() {
        return instance;
    }
}
