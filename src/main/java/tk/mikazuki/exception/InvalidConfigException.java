package tk.mikazuki.exception;

/**
 * Created by Mikazuki on 2015/05/10.
 */
public class InvalidConfigException extends Exception {

    public InvalidConfigException() {
    }

    public InvalidConfigException(String message) {
        super(message);
    }
}
