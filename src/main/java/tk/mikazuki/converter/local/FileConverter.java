package tk.mikazuki.converter.local;

import tk.mikazuki.IntelliJapanese;

import java.io.*;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class FileConverter extends LocalConverter {

    private final String url;

    public FileConverter(String url) {
        this.url = IntelliJapanese.getInstance().getConfigDir() + url;
        this.map = new HashMap<>();
        this.reserved = new HashMap<>();

        File file = new File(this.url);

        if (!file.exists()) {
            this.generateDictionary();
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#") || line.length() == 0) {
                    continue;
                }
                this.map.put(Pattern.compile(line.split("\t")[0]), line.split("\t")[1]);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generateDictionary() {
        (new File(new File(this.url).getParent())).mkdir();
        try (InputStream is = this.getClass().getResourceAsStream("/dictionary.txt");
             BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(this.url)),
                                                                           "UTF-8"))) {
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(line + "\n");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
