package tk.mikazuki.converter.local;

import org.spongepowered.api.Game;
import org.spongepowered.api.service.sql.SqlService;
import tk.mikazuki.IntelliJapanese;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class DatabaseConverter extends LocalConverter {
    // Database is using H2(Sponge Recommended)

    // CREATE TABLE DICTIONARY(TARGET VARCHAR(255) UNIQUE, REPLACED VARCHAR(255));
    private final String url;

    private final SqlService sqlService;

    public DatabaseConverter(Game game, String url) {
        this.sqlService = game.getServiceManager().provide(SqlService.class).get();
        this.url = IntelliJapanese.getInstance().getConfigDir() + url;
        this.map = new HashMap<>();
        this.reserved = new HashMap<>();

        File file = new File(this.url);
        if (!file.exists()) {
            this.generateDictionary();
        }

        // clean
        url = this.url.replace(".mv.db", "");
        url = url.replace(".h2.db", "");

        try (Connection connection = this.sqlService.getDataSource("jdbc:h2:" + url).getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT TARGET, REPLACED FROM DICTIONARY");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                this.map.put(Pattern.compile(resultSet.getString("TARGET")), resultSet.getString("REPLACED"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void refresh() {
        this.map.clear();

        String url = this.url.replace(".mv.db", "");
        url = url.replace(".h2.db", "");

        try (Connection connection = this.sqlService.getDataSource("jdbc:h2:" + url).getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT TARGET, REPLACED FROM DICTIONARY");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                this.map.put(Pattern.compile(resultSet.getString("TARGET")), resultSet.getString("REPLACED"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void generateDictionary() {
        (new File(new File(this.url).getParent())).mkdir();
        try {
            InputStream is = this.getClass().getResourceAsStream("/dictionary.mv.db");
            FileOutputStream fos = new FileOutputStream(new File(this.url));

            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) != -1) {
                fos.write(buffer, 0, length);
            }
            fos.close();
            is.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}