package tk.mikazuki.converter.local;

import tk.mikazuki.IntelliJapanese;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class LocalConverter {

    protected Map<Pattern, String> map;
    protected Map<String, String> reserved;

    /**
     * ひらがな変換前の、ローマ字状態の文字列を返します。
     *
     * @param text
     * @return
     */
    public String getRomaString(String text) {

        int counter = 0;
        for (Object obj : this.map.keySet()) {
            Pattern pattern = (Pattern) obj;
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                if (this.ignoreCheck(text)) {
                    continue;
                }
                String replaced;
                if (this.map.get(pattern).toString().equalsIgnoreCase("$")) {
                    replaced = matcher.group();
                }
                else {
                    replaced = this.map.get(pattern).toString();
                }
                text = matcher.replaceAll("rsvd" + counter);
                this.reserved.put("rsvd" + (counter++), replaced);
            }
        }
        return text;
    }

    private boolean ignoreCheck(String text) {
        for (Pattern pattern1 : IntelliJapanese.getInstance().getConfig().getIgnoredConfig().getPatterns()) {
            if (pattern1.matcher(text).find()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 最終変換結果を返します。
     *
     * @param text
     * @return
     */
    public String getConvertedString(String text) {
        for (String string : this.reserved.keySet()) {
            text = text.replaceAll(string, this.reserved.get(string));
        }
        this.reserved.clear();
        return text;
    }
}