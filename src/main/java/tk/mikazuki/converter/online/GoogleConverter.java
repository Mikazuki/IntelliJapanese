package tk.mikazuki.converter.online;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class GoogleConverter extends OnlineConverter {

    @Override
    public String getString(String text) {

        try {
            URL url = new URL("http://www.google.com/transliterate?langpair=ja-Hira|ja&text=" + URLEncoder.encode(text,
                                                                                                                  "UTF-8"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.setRequestMethod("GET");
            try (InputStream inputStream = connection.getInputStream()) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line = bufferedReader.readLine();
                connection.disconnect();

                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse(line).getAsJsonArray();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < array.size(); i++) {
                    sb.append(array.get(i).getAsJsonArray().get(1).getAsJsonArray().get(0).getAsString());
                }

                String value = sb.toString();
                value = value.replaceAll("０", "0");
                value = value.replaceAll("１", "1");
                value = value.replaceAll("２", "2");
                value = value.replaceAll("３", "3");
                value = value.replaceAll("４", "4");
                value = value.replaceAll("５", "5");
                value = value.replaceAll("６", "6");
                value = value.replaceAll("７", "7");
                value = value.replaceAll("８", "8");
                value = value.replaceAll("９", "9");
                return value;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
}