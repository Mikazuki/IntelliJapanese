package tk.mikazuki.converter.online;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class YahooConverter extends OnlineConverter {

    private final String appId;

    public YahooConverter(String appId) {
        this.appId = appId;
    }

    @Override
    public String getString(String text) {
        return text;
    }
}
