package tk.mikazuki.converter.online;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class SocialConverter extends OnlineConverter {
    @Override
    public String getString(String text) {
        try {
            URL url = new URL("http://www.social-ime.com/api/?string=" + URLEncoder.encode(text,
                                                                                           "UTF-8") + "&charset=UTF-8");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            try (InputStream inputStream = connection.getInputStream();
                 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"))) {

                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.split("\t").length == 0) {
                        stringBuilder.append(" ");
                    }
                    else {
                        stringBuilder.append(line.split("\t")[0]);
                    }
                }

                return stringBuilder.toString();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
}
