package tk.mikazuki.converter.online;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public abstract class OnlineConverter {

    public abstract String getString(String text);
}
