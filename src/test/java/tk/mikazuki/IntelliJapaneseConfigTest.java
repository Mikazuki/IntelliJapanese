package tk.mikazuki;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mikazuki on 2015/05/09.
 */
public class IntelliJapaneseConfigTest {

    private File file = new File("./IntelliJapanese.conf");
    private IntelliJapaneseConfig config;

    @Before
    public void beforeTest() throws Exception {
        IntelliJapanese intelliJapanese = new IntelliJapanese();

        Field field = intelliJapanese.getClass().getDeclaredField("defaultConfig");
        field.setAccessible(true);
        field.set(intelliJapanese, this.file);
        System.out.println("FILE OUTPUT >> " + this.file.getAbsoluteFile());

        intelliJapanese.onPreInitialization(null);
        this.config = IntelliJapanese.getInstance().getConfig();
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void testConfigTest() throws Exception {
        assertEquals(false, this.config.getDatabaseConfig().isEnable());
        assertEquals("/intellijapanese/dictionary.mv.db", this.config.getDatabaseConfig().getUrl());
        assertEquals("mikazuki", this.config.getDatabaseConfig().getUsername());
        assertEquals("", this.config.getDatabaseConfig().getPassword());
        assertEquals(true, this.config.getTextFileConfig().isEnable());
        assertEquals("/intellijapanese/dictionary.txt", this.config.getTextFileConfig().getUrl());
        assertEquals(true, this.config.getGoogleConfig().isEnable());
        assertEquals(false, this.config.getYahooConfig().isEnable());
        assertEquals("Your AppID", this.config.getYahooConfig().getAppid());
        assertEquals(false, this.config.getSocialConfig().isEnable());
        assertEquals(false, this.config.getMikugoConfig().isEnable());

    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @After
    public void afterTest() {
        this.file.delete();
    }
}
