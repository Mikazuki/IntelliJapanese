package tk.mikazuki;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("SpellCheckingInspection")
public class RomaToHiraTest {

    private File file = new File("./IntelliJapanese.conf");

    @Before
    public void beforeTest() throws Exception {
        IntelliJapanese intelliJapanese = new IntelliJapanese();

        Field field = intelliJapanese.getClass().getDeclaredField("defaultConfig");
        field.setAccessible(true);
        field.set(intelliJapanese, this.file);
        System.out.println("FILE OUTPUT >> " + this.file.getAbsoluteFile());

        intelliJapanese.onPreInitialization(null);
    }

    @Test
    public void testGetConvertText() throws Exception {
        String roma = "hibikidayo";
        String hira = "ひびきだよ";

        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "houhou,humuhumu!";
        hira = "ほうほう,ふむふむ!";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "sa-a,hajimetyaimasuka!";
        hira = "さ-あ,はじめちゃいますか!";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "gorennsousansogyorai!ittyattele-!";
        hira = "ごれんそうさんそぎょらい!いっちゃってぇ-!";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "olo-.iinele.";
        hira = "おぉ-.いいねぇ.";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "njyalaa-,sorosorohonkidala-ssu!!";
        hira = "んじゃぁあ-,そろそろほんきだぁ-っす!!";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));

        roma = "http://sponge.mikazuki.tk/IntelliJapanese.html";
        hira = "http://sponge.mikazuki.tk/IntelliJapanese.html";
        assertEquals(hira, RomaToHira.getSingleton().getConvertText(roma));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @After
    public void afterTest() {
        this.file.delete();
    }
}