package tk.mikazuki.converter.online;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tk.mikazuki.IntelliJapanese;
import tk.mikazuki.RomaToHira;
import tk.mikazuki.converter.local.FileConverter;

import java.io.File;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mikazuki on 2015/05/12.
 */
public class SocialConverterTest {
    private File file = new File("./IntelliJapanese.conf");
    private FileConverter localConverter;
    private SocialConverter onlineConverter;

    @Before
    public void beforeTest() throws Exception {
        IntelliJapanese intelliJapanese = new IntelliJapanese();

        Field field = intelliJapanese.getClass().getDeclaredField("defaultConfig");
        field.setAccessible(true);
        field.set(intelliJapanese, this.file);
        System.out.println("FILE OUTPUT >> " + this.file.getAbsoluteFile());

        intelliJapanese.onPreInitialization(null);
    }

    @Test
    public void testGoogleConverter() throws Exception {
        this.localConverter = new FileConverter("/intellijapanese.txt");
        this.onlineConverter = new SocialConverter();

        // 純粋な... なんか変換が変
        assertEquals("艦隊コレクション 艦これ", onlineConverter.getString("かんたいこれくしょん かんこれ"));

        // 変換を通す
        assertEquals("響だよ", convert("hibikidayo"));
        assertEquals("ここでは着物を脱ぐ", convert("kokodehakimonowonugu"));
        assertEquals("@Hello World", convert("@Hello World"));

        // 文章... Yahoo! 日本語係り受け解析APIで文節区切りとかするべきなのかもしれない
        assertEquals("子のプラグインは、日本誤変換を行います。", convert("konopuraguinha,nihongohenkanwookonaimasu."));
    }

    private String convert(String v) {
        v = this.localConverter.getRomaString(v);
        v = RomaToHira.getSingleton().getConvertText(v);
        v = this.onlineConverter.getString(v);
        return this.localConverter.getConvertedString(v);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @After
    public void afterTest() {
        this.file.delete();
        new File(this.file.getParent() + "/intellijapanese.txt").delete();
    }
}
