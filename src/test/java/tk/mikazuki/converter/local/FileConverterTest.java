package tk.mikazuki.converter.local;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tk.mikazuki.IntelliJapanese;
import tk.mikazuki.RomaToHira;

import java.io.File;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mikazuki on 2015/05/09.
 */
@SuppressWarnings("SpellCheckingInspection")
public class FileConverterTest {
    private File file = new File("./IntelliJapanese.conf");
    private FileConverter converter;

    @Before
    public void beforeTest() throws Exception {
        IntelliJapanese intelliJapanese = new IntelliJapanese();

        Field field = intelliJapanese.getClass().getDeclaredField("defaultConfig");
        field.setAccessible(true);
        field.set(intelliJapanese, this.file);
        System.out.println("FILE OUTPUT >> " + this.file.getAbsoluteFile());

        intelliJapanese.onPreInitialization(null);
    }

    @Test
    public void testFileCoverter() throws Exception {
        this.converter = new FileConverter("/intellijapanese.txt");

        assertEquals("ひびきだよ", convert("hibikidayo"));
        assertEquals("Minecraftだよ", convert("Minecraftdayo"));
        assertEquals("んじゃぁあー、そろそろほんきだぁーっす!!", convert("njyalaa-,sorosorohonkidala-ssu!!"));
        assertEquals("http://mikazuki.tk", convert("http://mikazuki.tk"));
        assertEquals("(´・ω・`)", convert("kao1"));
        assertEquals("@Hello World!", convert("@Hello World!"));
    }

    private String convert(String v) {
        v = converter.getRomaString(v);
        v = RomaToHira.getSingleton().getConvertText(v);
        return converter.getConvertedString(v);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @After
    public void afterTest() {
        this.file.delete();
        new File(this.file.getParent() + "/intellijapanese.txt").delete();
    }
}
